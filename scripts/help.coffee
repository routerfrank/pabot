_ = require("lodash")

module.exports =
  _.template(
    "It's really easy to speak my language, tell me what you want with any of the following commands and I shall get back to you right away"  + "\n" + "\n" +
    "`information` - Introduces self" + "\n"  +
    "`weather in <city>` - Get currenty weather" + "\n" +
    "`weather forecast in <city>` - Get 7 days weather forecast" + "\n" +
    "`note that <note content>` - Create a new note" + "\n" +
    "`list notes` - Show the list of notes" + "\n"  +
    "`delete note <note number>` - Delete note" + "\n"  +
    "`mentor in <technology>` - Add you as a mentor" + "\n"  +
    "`list mentors` - Get the list of mentors" + "\n"  +
    "`not mentor in <technology>` - Remove you as mentor" + "\n"  +
    "`stuck on <technology> with issue: <issue description>` - Get help from mentors" + "\n"  +
    "`calculate or convert <expression>` - Do mathematical calculator or unit conversion" + "\n"  +
    "`twitter` - Show instruction on setting up twitter on Slack" + "\n"  +
    "`list bots` - Get the list of bots on Andela Slack" + "\n" +
    "`translate <words>` - Translate to English" + "\n" +
    "`translate from <language> into <language> <words>` - Translate to specified language" + "\n" +
    "`remind me tomorrow to document this better`" + "\n" +
    "`remind at 5 PM to go home`" + "\n" +
    "`remind in every 30 minutes to take a walk`"  + "\n" +
    "`delete reminder <number>` - Delete a reminder" + "\n" +
    "`list reminders` - Show all reminders" + "\n" +
    "\n" + "\n" +
    "```" + "\n" +
        "If you do not get an immediate response from me, I have gone on a coffee break (yes, I am powered by coffee). I promise to be back soon." + "\n" +
    "```"
  )
