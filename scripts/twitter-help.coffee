_ = require("lodash")

module.exports =
  _.template(
    "1. Goto https://apps.twitter.com/\n" +
    "2. Click on `Create New App` on the upper right coner of the page\n" +
    "3. Enter `Genio - <your slack username>` as the app name\n" +
    "4. Enter a little description\n" +
    "5. Enter http://www.genio.com/ in the Website field\n" +
    "6. Leave the Callback URL field empty\n" +
    "7. Check Yes, I agree and click Submit\n" +
    "8. Navigate to Keys and Access Tokens tab\n" +
    "9. Scroll down the page and click `Create my access token`\n" +
    "10. Now copy your `Consumer Key (API Key)`, `Consumer Secret (API Secret)`, " +
    "`Access Token` and `Access Token Secret` and save them somewhere\n" +
    "11. Then set them in your `environment variables` in this format:\n\n" +
    "`export HUBOT_TWITTER_CONSUMER_KEY='Consumer Key (API Key)'`\n" +
    "`export HUBOT_TWITTER_CONSUMER_SECRET='Consumer Secret (API Secret)'`\n" +
    "`export HUBOT_TWEETER_ACCOUNTS='{ \"<your twitter handle>\" : {\"access_token\" : \"<Access Token>\", \"access_token_secret\" : \"<Access Token Secret>\"} }'`" +
    "\n\nCommands:\n" +
    "`tweet@<screen_name> <your tweet>` - tweet @ <screen_name>\n" +
    "`untweet@<screen_name> <tweet_url_or_id>` - deletes tweet from Twitter\n" +
    "`retweet@<screen_name> <tweet_url_or_id>` - <screen_name> retweets tweet\n" +
    "`Alias: rt@<screen_name>`"
  )

# '{ "<your twitter handle>" : {"access_token" : "<Access Token>", "access_token_secret" : "<Access Token Secret>"} }'