fuzzy = require "fuzzy"

module.exports =

  fuzzyMatch: (tech, technologies) ->
    return null if !tech and !technologies
    results = fuzzy.filter tech, technologies
    matches = results.map (el) ->
      return el.string
    if matches[0]
      return matches[0]
    else
      return tech
      
  # when retrieving from firebase
  formatTextFromFirebase: (text) ->
    return null if !text
    formattedText = text.replace(/\,/g, '.').replace(/\@/g, '#').replace(/\&/g, '$')
    return formattedText

  # when saving to firebase
  formatTextForFirebase: (text) ->
    return null if !text
    formattedText = text.replace(/\./g, ',').replace(/\#/g, '@').replace(/\$/g, '&').replace(/[|]/g,'')
    return formattedText