env           = process.env.NODE_ENV || "development"
say           = require "say"
_             = require "lodash"
moment        = require "moment"
mathjs        = require "mathjs"
daysOfTheWeek = require("./days")()
formatter     = require "./formatter"
config        = require("../config/config")[env]
StateMachine  = require "javascript-state-machine"


module.exports = (rootRef, id) ->
  rootRef = rootRef
  id = id
  StateMachine.create
    initial: "initial"
    events: [
      {
        name: "currentweather"
        from: "initial"
        to: "getCurrentWeather"
      }
      {
        name: "weeklyweather"
        from: "initial"
        to: "getWeeklyWeather"
      }
      {
        name: "createnote"
        from: "initial"
        to: "makeNote"
      }
      {
        name: "listnote"
        from: "initial"
        to: "getNotes"
      }
      {
        name: "deletenote"
        from: "initial"
        to: "removeNote"
      }
      {
        name: "compute"
        from: "initial"
        to: "calculate"
      }
      {
        name: "mentor"
        from: "initial"
        to: "newMentor"
      }
      {
        name: "removementor"
        from: "initial"
        to: "removeMentor"
      }
      {
        name: "stuck"
        from: "initial"
        to: "getMentors"
      }
      {
        name: "listmentors"
        from: "initial"
        to: "listMentors"
      }
      {
        name: "allbots"
        from: "initial"
        to: "listbots"
      }
      {
        name: "greeting"
        from: "initial"
        to: "greetingMessage"
      }
      {
        name: "reset"
        from: [
          "getCurrentWeather"
          "getWeeklyWeather"
          "getNotes"
          "makeNote"
          "removeNote"
          "calculate"
          "newMentor"
          "getMentors"
          "listMentors"
          "listbots"
          "removeMentor"
          "greetingMessage"
        ]
        to: "initial"
      }
    ]
    callbacks:
      oncurrentweather: (event, from, to, robot, city, cb) ->
        url = "http://api.openweathermap.org/data/2.5/weather"
        query = {units: "metric", q: city}
        robot.http(url).query(query).get() (err, response, body) ->
          data = JSON.parse(body)
          if data.cod isnt 200
            message = "I can only find weather for places on planet Earth"
            cb message
            say.speak null, message
          else if data.cod is 200
            desc = data.weather[0].description
            temp = Math.round(data.main.temp)
            humidity = data.main.humidity
            message = "It's #{temp}°, #{desc} with humidity of #{humidity}% in #{data.name}, #{data.sys.country}"
            cb message
            say.speak null, message

      onweeklyweather: (event, from, to, robot, city, cb) ->
        url = "http://api.openweathermap.org/data/2.5/forecast/daily"
        query = {units: "metric", cnt: 7, q: city}
        message = ""
        robot.http(url).query(query).get() (err, response, body) ->
          data = JSON.parse(body)
          if data.cod isnt "200"
            message += "I can only forecast weather for places on planet Earth"
            cb(message)
          else if data.cod is "200"
            _.forEach data.list, (weather, key) ->
              date = new Date(weather.dt * 1000)
              day = daysOfTheWeek[date.getDay()]
              humidity = weather.humidity
              minTemp = Math.round(weather.temp.min)
              maxTemp = Math.round(weather.temp.max)
              desc = weather.weather[0].description
              message += "#{day}: #{minTemp}° / #{maxTemp}°, #{desc} with humidity of #{humidity}%\n"
            cb message

      oncreatenote: (event, from, to, user, note, cb) ->
        date = moment().format("MMM Do YY")
        time = moment().format("h:mm:ss a")
        rootRef.child("notes").child(user).child(date).child(time).set note
        message = "note saved"
        cb message
        say.speak null, message

      onlistnote: (event, from, to, user, cb) ->
        rootRef.child("notes").child(user).once "value", (snap) ->
          if snap.val()
            message = ""
            index = 1
            _.forEach snap.val(), (dateObject, date) ->
              message += "#{date}\n"
              _.forEach dateObject, (note, time) ->
                message += ">#{index}. #{note} @ #{time}\n"
                index++
            false
            cb message
          else
            message = "You don't have note, create one"
            cb message
            say.speak null, message

      ondeletenote: (event, from, to, user, index, cb) ->
        rootRef.child("notes").child(user).once "value", (snap) ->
          if snap.val()
            counter = 1
            invalidIndex = true
            message = ""
            _.forEach snap.val(), (dateObject, date) ->
              _.forEach dateObject, (note, time) ->
                if counter is index
                  rootRef.child("notes").child(user).child(date).child(time).remove()
                  message = "deleted note #{index}"
                  invalidIndex = false
                ++counter
            cb message
            say.speak null, message
            if invalidIndex
              message = "No such note to delete. Use the `list notes` command to see a list of existing notes"
              cb message
          else
            message = "there are no notes to delete"
            cb message
            say.speak null, message

      oncompute: (event, from, to, expression, cb) ->
        try
          result = mathjs.eval expression
          cb "#{result}"
        catch error
          message = error.message or "Could not compute."
          cb message

      onmentor: (event, from, to, user, tech, cb) ->
        rootRef.child("mentors").once "value", (snap) ->
          techs = []
          snap.forEach (techSnap) ->
            techs.push techSnap.key()
            false
          techMatch = formatter.fuzzyMatch tech, techs
          if techs.indexOf(techMatch) > -1
            rootRef.child("mentors").child(techMatch).child(user).set true
            tech = formatter.formatTextFromFirebase techMatch
            message = "You have been added as a mentor in #{tech}, thank you."
            cb message
            say.speak null, message
          else
            message = "Nobody needs help in #{formatter.formatTextFromFirebase tech}, thank you."
            cb message
            say.speak null, message

      onremovementor: (event, from, to, user, tech, cb) ->
        rootRef.child("mentors").once "value", (snap) ->
          techs = []
          snap.forEach (techSnap) ->
            techs.push techSnap.key()
            false
          techMatch = formatter.fuzzyMatch tech, techs
          if techs.indexOf(techMatch) > -1
            formattedTech = formatter.formatTextFromFirebase techMatch
            rootRef.child("mentors").child(techMatch).child(user).remove()
            message = "You have been remove as mentor in #{formattedTech}, Thank you."
            cb message
            say.speak null, message
          else
            message = "You are not a mentor in #{tech}, thank you."
            cb message
            say.speak null, message


      onlistmentors: (event, from, to, cb) ->
        rootRef.child("mentors").once "value", (snap) ->
          message = ""
          _.forEach snap.val(), (key, tech) ->
            message += "*#{tech}*\n" if Object.keys(key).length > 1
            if typeof key is "object"
              _.forEach key, (val, mentor) ->
                message += ">#{mentor}\n" if mentor isnt "mentors"
          cb formatter.formatTextFromFirebase message

      onstuck: (event, from, to, robot, user, tech, issue, cb) ->
        rootRef.child("mentors").once "value", (snap) ->
          techs = []
          snap.forEach (techSnap) ->
            techs.push techSnap.key()
            false
          techMatch = formatter.fuzzyMatch tech, techs
          if techs.indexOf(techMatch) > -1
            rootRef.child("mentors").child(techMatch).once "value", (snap) ->
              if Object.keys(snap.val()).length > 1
                link = ""
                robot.http('http://randomword.setgetgo.com/get.php').get() (err, re, body) ->
                  link = 'http://appear.in/' + body.trim().toLowerCase() + "/"
                  _.forEach snap.val(), (value, helper) ->
                    mentor = {room: helper}
                    formattedTech = formatter.formatTextFromFirebase techMatch
                    message = "#{user} is stuck on #{formattedTech} with issue: \n```#{issue}```\n>Kindly Join the helper conference by clicking the link below.\n #{link}"
                    robot.send mentor, message if helper isnt "mentors"
                  message = "I just asked the mentors to help you out. Join helpers conference by clicking the link below.\n#{link}"
                  cb message
              else
                tech = formatter.formatTextFromFirebase tech
                message = "No mentor is available in #{tech}. type `list mentors` to see a list of available mentors"
                cb message
          else
            tech = formatter.formatTextFromFirebase tech
            message = "#{tech} have not been added to the list of technologies"
            cb message

      onallbots: (event, from, to, robot, cb) ->
        url = "https://slack.com/api/users.list?token=" + config.skilltree_token
        message = ""
        counter = 1
        robot.http(url).get() (err, response, body) ->
          data = JSON.parse(body)
          _.forEach data.members, (member) ->
            if member.is_bot is true
              message += "#{counter}. #{member.name}\n"
              counter++
          numOfBots = "\nCurrently Andela has #{counter-1} bots"
          cb message + numOfBots

      ongreeting: (event, from, to, user, cb) ->
        responses = [
          "Hey #{user}, I know you didn't just come to say hi, so how may I help you? Type `help` to see how I can make your life easier", 
          "Whataguan #{user}? Type `help` to see how I can make your life easier", 
          "Hi #{user}, what's popping in tha hood? Type `help` to see how I can make your life easier", 
          "Hello, #{user}, I have been expecting you? Type `help` to see how I can make your life easier"
        ]
        response = Math.floor(Math.random() * responses.length)
        message = responses[response]
        cb message

      onreset: (event, from, to) ->
        console.log "reset state"
