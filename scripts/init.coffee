_ = require("lodash")
genio = require('./genio')

module.exports =

  run: (rootRef, genios) ->
    rootRef.child('people').once 'value', (snap) ->
      _.forEach snap.val(), (person) ->
        if person.slack_id
          id = person.slack_id
          genios[id] = new genio rootRef, id