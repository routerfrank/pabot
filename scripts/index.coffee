authenticate = require "./authentication"
genio = require "./genio"
twitterHelper = require "./twitter-help"
init = require "./init"
helper = require "./help"
rootRef = null
genios = {}
fs = require "fs"
formatter     = require "./formatter"

module.exports = (robot) ->

  authenticate.firebase (err, ref) ->
    if !err
      rootRef = ref
      rootRef.child("people").once "value", (snap) ->
        init.run rootRef, genios

  robot.respond /add tech (.*)/i, (res) ->
    tech = formatter.formatTextForFirebase res.match[1]
    admin = res.message.user.name
    rootRef.child("admins").child(admin).once "value", (snap) ->
      if snap.val()
        rootRef.child("mentors").child(tech).child("mentors").set "bool"
        rese.send "#{res.match[1]} have been added to the list of technologies"
      else
        res.send "Mind your business"

  robot.respond /(.*)information?/i, (res) ->
    res.send "Genio, Genius, Intelligence!"

  robot.respond /(Hello|Hi)/i, (res) ->
    user = res.message.user.name
    genios[user].greeting user, (message) ->
      res.send message
      genios[user].current = "initial"

  robot.respond /help/i, (res) ->
    commands = helper()
    res.send commands

  robot.respond /weather in (.*)/i, (res) ->
    location = res.match[1]
    user = res.message.user.name
    getWeather user, location, res

  robot.respond /weather forecast in (.*)/i, (res) ->
    location = res.match[1]
    user = res.message.user.name
    genios[user].weeklyweather robot, location, (message) ->
      res.send message
      genios[user].current = "initial"

  robot.respond /note that (.*)/i, (res) ->
    note = res.match[1]
    user = res.message.user.name
    writeNote user, note, res

  robot.respond /(all|list) (note|notes)/i, (res) ->
    user = res.message.user.name
    listNotes user, res

  robot.respond /delete note (.*)/i, (res) ->
    user = res.message.user.name
    index = Number(res.match[1])
    deleteNote user, index, res

  robot.respond /stuck on (.*) with issue: (.*)/i, (res) ->
    user = res.message.user.name
    tech = formatter.formatTextForFirebase res.match[1]
    issue = res.match[2]
    needHelp user, tech, issue, robot, res

  robot.respond /mentor in (.*)/i, (res) ->
    user = res.message.user.name
    tech = formatter.formatTextForFirebase res.match[1]
    registerAsMentor user, tech, res

  robot.respond /not mentor in (.*)/i, (res) ->
    user = res.message.user.name
    tech = formatter.formatTextForFirebase res.match[1]
    genios[user].removementor user, tech, (message) ->
      res.send message
      genios[user].current = "initial"

  robot.respond /(calc|calculate|calculator|convert|math|maths)( me)? (.*)/i, (res) ->
    expression = res.match[3]
    user = res.message.user.name
    calculate user, expression, res

  robot.respond /(twitter help|twitter)/i, (res) ->
    commands = twitterHelper()
    res.send commands

  robot.respond /(all|list) (mentor|mentors)/i, (res) ->
    user = res.message.user.name
    listmentors user, res

  robot.respond /(all|list) (bots|robots|robot)/i, (res) ->
    user = res.message.user.name
    genios[user].allbots robot, (message) ->
      res.send message
      genios[user].current = "initial"

  fs.exists './logs/', (exists) ->
    if exists
      startLogging()
    else
      fs.mkdir './logs/', (error) ->
      unless error
        startLogging()
      else
        console.log "Could not create logs directory: #{error}"
  startLogging = ->
    console.log "Started logging"
    robot.hear //, (msg) ->
      fs.appendFile logFileName(msg), formatMessage(msg), (error) ->
        console.log "Could not log message: #{error}" if error
  logFileName = (msg) ->
    safe_room_name = "#{msg.message.room}".replace /[^a-z0-9]/ig, ''
    "./logs/#{safe_room_name}.log"
  formatMessage = (msg) ->
    "[#{new Date()}] #{msg.message.user.name}: #{msg.message.text}\n"


  robot.router.post "/incoming", (req, res) ->
    body = req.body
    if body.command == "getweather"
      getWeather body.slack_id, body.location, res

    else if body.command == "listnotes"
      listNotes body.slack_id, res

    else if body.command == "writenote"
      writeNote body.slack_id, body.note, res

    else if body.command == "deletenote"
      deleteNote body.slack_id, Number(body.index), res

    else if body.command == "needhelp"
      needHelp body.slack_id, body.tech, body.desc, res

    else if body.command == "mentor"
      registerAsMentor body.slack_id, body.tech, res

    else if body.command == "calculate"
      calculate body.slack_id, body.expression, res

    else if body.command == 'listmentors'
      listmentors body.slack_id, res


  getWeather = (user, location, res) ->
    genios[user].currentweather robot, location, (message) ->
      res.send message
      genios[user].current = "initial"

  listNotes = (user, res) ->
    genios[user].listnote user, (message) ->
      res.send message
      genios[user].current = "initial"

  writeNote = (user, note, res) ->
    genios[user].createnote user, note, (message) ->
      res.send message
      genios[user].current = "initial"

  deleteNote = (user, index, res) ->
    genios[user].deletenote user, index, (message) ->
      res.send message
      genios[user].current = "initial"

  needHelp = (user, tech, issue, robot, res) ->
    genios[user].stuck robot, user, tech, issue, (message) ->
      res.send message
      genios[user].current = "initial"

  registerAsMentor = (user, tech, res) ->
    genios[user].mentor user, tech, (message) ->
      res.send message
      genios[user].current = "initial"

  calculate = (user, expression, res) ->
    genios[user].compute expression, (message) ->
      res.send message
      genios[user].current = "initial"

  listmentors = (user, res) ->
    genios[user].listmentors (message) ->
      res.send message
      genios[user].current = 'initial'
